.SILENT:
BUILDDIR=build
CHARTSDIR=charts
CHARTS=$(CHARTSDIR)/*.tex
BUILDDEPS=$(BUILDDIR)/tzflowchart.sty $(BUILDDIR)/xmppdoc.sty $(CHARTS)
DO_TEX = xelatex -output-directory=$(BUILDDIR) $<

flowcharts.pdf: flowcharts.tex tzflowchart.sty $(BUILDDEPS)
	@echo "Compiling using $(DO_TEX)"
	$(DO_TEX)
	makeglossaries -d $(BUILDDIR) $(basename $<)
	while ($(DO_TEX) ; \
	grep -q "Rerun to get" $(BUILDDIR)/$<.log ) do true ; \
	done
	# Run one more time in case it has already been built
	$(DO_TEX)
	
	mv $(BUILDDIR)/$@ $@

.PHONY: clean
clean:
	rm -rf $(BUILDDIR)
	rm -f *.svg
	rm -f *.pdf

$(BUILDDIR):
	mkdir -p $@

$(BUILDDIR)/tzflowchart.sty: tzflowchart.sty $(BUILDDIR)
	cp $< $@

$(BUILDDIR)/xmppdoc.sty: xmppdoc.sty $(BUILDDIR)
	cp $< $@

%.svg: CHARTNAME = $(basename $@)
%.svg: BUILDFILE=$(CHARTNAME).tex
%.svg: OUTFILE=$(CHARTNAME).svg
%.svg: $(BUILDDEPS)
	hash pdf2svg 2>/dev/null || { echo >&2 "SVG output requires that pdf2svg be installed"; exit 1; }
	cp svgtemplate.tex $(BUILDDIR)/$(BUILDFILE)
	cd $(BUILDDIR); xelatex -halt-on-error -shell-escape $(BUILDFILE)
	mv $(BUILDDIR)/$(OUTFILE) $(OUTFILE)

%.pdf: ARTICLENAME = $(basename $@)
%.pdf: BUILDFILE=$(ARTICLENAME).tex
%.pdf: OUTFILE=$(ARTICLENAME).pdf
%.pdf: DO_BUILD_ARTICLE = cd $(BUILDDIR); xelatex -halt-on-error $(BUILDFILE)
%.pdf: $(BUILDDEPS)
	cp articletemplate.tex $(BUILDDIR)/$(BUILDFILE)
	cp glossary.tex build/glossary.tex
	cp -r charts/ build/
	$(DO_BUILD_ARTICLE)
	cd $(BUILDDIR); makeglossaries $(ARTICLENAME)
	while ($(DO_BUILD_ARTICLE) ; \
	grep -q "Rerun to get" $(BUILDDIR)/$<.log ) do true ; \
	done
	mv $(BUILDDIR)/$(OUTFILE) $(OUTFILE)
