# XMPP Docs

This is my attempt to build simple (mostly flowchart based) documentation for
implementing XMPP that will (hopefully) be easily accessible to a broad range of
developers.

## License

This text is licensed under a Creative Commons Attribution 4.0 International
license ([CC BY 4.0][ccby]).

[ccby]: https://creativecommons.org/licenses/by/4.0/
